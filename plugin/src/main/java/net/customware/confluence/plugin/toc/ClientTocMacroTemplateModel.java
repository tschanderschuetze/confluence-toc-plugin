package net.customware.confluence.plugin.toc;

import com.atlassian.fugue.Function2;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.Range;

import java.util.Collection;
import java.util.Map;

import static java.lang.Boolean.parseBoolean;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;
import static org.apache.commons.lang3.Range.between;
import static org.apache.commons.lang3.StringUtils.defaultString;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;

/**
 * Responsible for constructing the SOY template model for the client-side macro implementation.
 */
class ClientTocMacroTemplateModel {
    private static final Collection<MacroParameterProcessor> PARAMETER_PROCESSORS = ImmutableList.of(
            singleDataAttribute("type", "structure"),
            singleDataAttribute("outline", "numberedoutline"),
            singleDataAttribute("style", "cssliststyle"),
            singleDataAttribute("indent", "csslistindent"),
            separatorDataAttributes("separator", "preseparator", "midseparator", "postseparator"),
            headingMinMaxProcessor("minLevel", "maxLevel", "headerelements"),
            singleDataAttribute("include", "includeheaderregex"),
            singleDataAttribute("exclude", "excludeheaderregex")
    );

    private static final int MIN_HEADER_LEVEL = 1;
    private static final int MAX_HEADER_LEVEL = 7; // not sure why 7, but the old server-side implementaiton used 7, so....
    private static final Range<Integer> HEADER_LEVEL_RANGE = between(MIN_HEADER_LEVEL, MAX_HEADER_LEVEL);

    /**
     * For the given macro parameters, build a template model for rendering the client-side soy template.
     */
    static ImmutableMap<String, Object> buildTemplateModel(final Map<String, String> macroParameters) {
        return ImmutableMap.of(
                "dataAttributes", processMacroParameters(macroParameters),
                "nonPrintable", !parseBoolean(defaultString(macroParameters.get("printable"), "true")),
                "customCssClass", defaultString(macroParameters.get("class"))
        );
    }

    private static Map<String, String> processMacroParameters(final Map<String, String> macroParameters) {
        final ImmutableMap.Builder<String, String> mappedParameters = ImmutableMap.builder();
        for (MacroParameterProcessor parameterProcessor : PARAMETER_PROCESSORS) {
            mappedParameters.putAll(parameterProcessor.getDataAttributes(macroParameters));
        }
        return mappedParameters.build();
    }

    private interface MacroParameterProcessor {
        Map<String, String> getDataAttributes(Map<String, String> macroParameters);
    }

    private static MacroParameterProcessor singleDataAttribute(final String macroParameterName, final String dataAttributeName) {
        return new MacroParameterProcessor() {
            @Override
            public Map<String, String> getDataAttributes(final Map<String, String> macroParameters) {
                final String parameterValue = macroParameters.get(macroParameterName);
                if (isBlank(parameterValue)) {
                    return emptyMap();
                } else {
                    return singletonMap(dataAttributeName, trimToEmpty(parameterValue));
                }
            }
        };
    }

    private static MacroParameterProcessor separatorDataAttributes(final String macroParameterName, final String preseparator, final String midseparator, final String postseparator) {
        return new MacroParameterProcessor() {
            @Override
            public Map<String, String> getDataAttributes(final Map<String, String> macroParameters) {
                final String parameterValue = macroParameters.get(macroParameterName);
                if (isBlank(parameterValue)) {
                    return emptyMap();
                }
                final SeparatorType separatorType = SeparatorType.valueOfSeparator(parameterValue);
                if (separatorType != null) {
                    return dataAttributes(separatorType.getPre(), separatorType.getMid(), separatorType.getPost());
                } else {
                    return dataAttributes("", parameterValue, "");
                }
            }

            private Map<String, String> dataAttributes(String pre, String mid, String post) {
                return ImmutableMap.of(preseparator, pre, midseparator, mid, postseparator, post);
            }
        };
    }

    private static MacroParameterProcessor headingMinMaxProcessor(final String minMacroParameterName, final String maxMacroParameterName, final String dataAttributeName) {
        return new MacroParameterProcessor() {
            @Override
            public Map<String, String> getDataAttributes(final Map<String, String> macroParameters) {
                final Function2<String, Integer, Integer> headingLevelParser = new Function2<String, Integer, Integer>() {
                    @Override
                    public Integer apply(final String parameterName, final Integer defaultValue) {
                        final Integer value = Integer.valueOf(defaultString(macroParameters.get(parameterName), String.valueOf(defaultValue)));
                        return HEADER_LEVEL_RANGE.contains(value) ? value : defaultValue;
                    }
                };

                final int min = headingLevelParser.apply(minMacroParameterName, MIN_HEADER_LEVEL);
                final int max = headingLevelParser.apply(maxMacroParameterName, MAX_HEADER_LEVEL);

                final ImmutableList.Builder<String> headerElements = ImmutableList.builder();
                for (int i = min; i <= max; ++i) {
                    headerElements.add("H" + i);
                }

                return ImmutableMap.of(dataAttributeName, Joiner.on(',').join(headerElements.build()));
            }
        };
    }
}
