/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.customware.confluence.plugin.toc;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.HtmlToXmlConverter;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import java.util.Map;

import static com.atlassian.confluence.macro.Macro.BodyType.RICH_TEXT;
import static com.atlassian.renderer.v2.RenderMode.ALL;

/**
 * This is a subclass of the TOC macro works exactly the same, except that it
 * considers the contents of the macro to be the source of it's headers.
 *
 * @author David Peterson
 */
public class TOCZoneMacro extends AbstractTOCMacro {

    private static final String LOCATION_PARAM = "location";
    private static final String TOP_LOCATION = "top";
    private static final String BOTTOM_LOCATION = "bottom";

    public TOCZoneMacro(StaxDocumentOutlineCreator staxDocumentOutlineCreator,
                        HtmlToXmlConverter htmlToXmlConverter,
                        SettingsManager settingsManager, LocaleManager localeManager,
                        I18NBeanFactory i18nBeanFactory, PageBuilderService pageBuilderService) {
        super(staxDocumentOutlineCreator, htmlToXmlConverter,
                settingsManager, localeManager, i18nBeanFactory, pageBuilderService);
    }

    /**
     * @return <code>"toc-zone"</code>.
     */
    @Override
    public String getName() {
        return "toc-zone";
    }

    @Override
    protected String getContent(Map<String, String> parameters, String body, ConversionContext conversionContext) {
        return body;
    }

    @Override
    protected String createOutput(Map<String, String> parameters, String body, String toc) {
        boolean atTop, atBottom;
        String where = parameters.get(LOCATION_PARAM);

        atTop = !BOTTOM_LOCATION.equals(where);
        atBottom = !TOP_LOCATION.equals(where);

        StringBuilder out = new StringBuilder();
        if (atTop)
            out.append(toc);

        out.append(body);

        if (atBottom)
            out.append(toc);

        return out.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.randombits.confluence.toc.AbstractTOCMacro#getDefaultType()
     */
    @Override
    protected String getDefaultType() {
        return LIST_TYPE;
    }

    @Override
    protected String getUnprintableHtml(String body) {
        /* Returns the macro body by default -- TOC-66 */
        return body;
    }

    @Override
    public RenderMode getBodyRenderMode() {
        return ALL;
    }

    @Override
    public boolean hasBody() {
        return true;
    }

    @Override
    public BodyType getBodyType() {
        return RICH_TEXT;
    }

}
