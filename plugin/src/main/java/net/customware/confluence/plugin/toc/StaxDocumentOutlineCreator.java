package net.customware.confluence.plugin.toc;

import com.atlassian.confluence.content.render.xhtml.StaxUtils;
import com.atlassian.confluence.content.render.xhtml.XhtmlConstants;
import com.atlassian.confluence.content.render.xhtml.XmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.XmlOutputFactory;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class that uses StAX to build a {@link DocumentOutline} from the heading elements in a XHTML page content. *
 */
public class StaxDocumentOutlineCreator {
    private static final Pattern HEADING_ELEMENT_PATTERN = Pattern.compile("[h|H]([1-6])");
    private static final Logger log = LoggerFactory.getLogger(StaxDocumentOutlineCreator.class);

    private final XmlEventReaderFactory xmlEventReaderFactory;
    private final XmlOutputFactory xmlFragmentOutputFactory;

    public StaxDocumentOutlineCreator(XmlEventReaderFactory xmlEventReaderFactory, @Qualifier("xmlFragmentOutputFactory") XmlOutputFactory xmlFragmentOutputFactory) {
        this.xmlEventReaderFactory = xmlEventReaderFactory;
        this.xmlFragmentOutputFactory = xmlFragmentOutputFactory;
    }

    public DocumentOutline getOutline(String pageContent) throws Exception {
        DepthFirstDocumentOutlineBuilder outlineBuilder = new EmptyLevelTrimmingDocumentOutlineBuilder();

        try {
            XMLEventReader reader = xmlEventReaderFactory.createXMLEventReader(new StringReader(pageContent), XhtmlConstants.STORAGE_NAMESPACES, false);

            int lastLevel = 1; // the level of the last heading added to the outline

            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement element = event.asStartElement();
                    Matcher matcher = HEADING_ELEMENT_PATTERN.matcher(element.getName().getLocalPart());
                    if (matcher.matches()) {
                        int headingLevel = Integer.valueOf(matcher.group(1));
                        String headingId = getHeadingId(element);
                        Document headingText = Jsoup.parseBodyFragment(getHeadingText(reader, headingLevel), "");
                        headingText.outputSettings().prettyPrint(false);

                        Elements headingLinks = headingText.select("a");
                        for (Element headingLink : headingLinks) {

                            // backwards-compatible behaviour of honouring anchors in headers
                            // see http://confluence.atlassian.com/display/DOC/Table+of+Contents+Macro#TableofContentsMacro-FilteredTableofContents
                            if (headingId == null) {
                                // the page doesn't say anything about it, but I'd assume the contract was to pick the first one
                                String anchorName = headingLink.attr("name");
                                if (StringUtils.isNotBlank(anchorName)) {
                                    headingId = anchorName;
                                }
                            }

                            // remove all links from the heading text, because nested links (we'll wrap the text in a link later) are illegal according to HTML4 spec
                            // and we also want to assert the uniqueness of potential anchors
                            for (Node childNode : new LinkedList<Node>(headingLink.childNodes())) {
                                headingLink.before(childNode);
                            }
                            headingLink.remove();
                        }

                        lastLevel = insertInBuilderStructure(headingLevel, headingText.body().text(), headingId, outlineBuilder, lastLevel);
                    }
                }
            }
        } catch (XMLStreamException ex) {
            log.error("Exception reading storage format data using an XMLEventReader", ex);
            throw new StaxOutlineBuilderException("Exception reading storage format data using an XMLEventReader", ex);
        }

        return outlineBuilder.getDocumentOutline();
    }

    private int insertInBuilderStructure(int level, String heading, String anchorText, DepthFirstDocumentOutlineBuilder builder, int lastLevel) {
        if (level < lastLevel) {
            for (int i = 0; i < lastLevel - level; i++) {
                builder.previousLevel();
            }
        } else if (level > lastLevel) {
            for (int i = 0; i < level - lastLevel; i++) {
                builder.nextLevel();
            }
        }

        builder.add(heading, anchorText, level);

        return level;
    }

    /**
     * Read the anchor from within a heading
     *
     * @param element heading element
     * @return the heading id or null if the heading does not have an id
     * @throws XMLStreamException
     */
    private String getHeadingId(StartElement element) throws XMLStreamException {
        Attribute headingId = element.getAttributeByName(new QName("id"));
        if (headingId != null) {
            return headingId.getValue();
        }
        return null;
    }

    /**
     * Take a reader that has just read a heading tag and keep reading the content of the heading until the end element for
     * the heading is encountered.
     *
     * @param reader heading reader
     * @return
     * @throws Exception
     */
    private String getHeadingText(XMLEventReader reader, int headingLevel) throws Exception {
        final Pattern endPattern = Pattern.compile("[h|H]" + headingLevel);

        StringWriter stringWriter = new StringWriter();
        XMLEventWriter eventWriter = xmlFragmentOutputFactory.createXMLEventWriter(stringWriter);

        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.isEndElement() && endPattern.matcher(event.asEndElement().getName().getLocalPart()).matches())
                break;

            eventWriter.add(event);
        }

        StaxUtils.flushEventWriter(eventWriter);
        return stringWriter.toString();
    }

    public class StaxOutlineBuilderException extends Exception {
        StaxOutlineBuilderException(String message, Throwable cause) {
            super(message, cause);
        }
    }

}
