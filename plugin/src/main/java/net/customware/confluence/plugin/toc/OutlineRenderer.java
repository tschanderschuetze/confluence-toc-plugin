package net.customware.confluence.plugin.toc;

import net.customware.confluence.plugin.toc.DocumentOutline.Heading;

import java.io.IOException;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

import static com.atlassian.confluence.util.HtmlUtil.htmlEncode;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * A class responsible for taking a DocumentOutline and rendering it to HTML in a required format
 * Not thread safe.
 */
public class OutlineRenderer {
    private final String cssClass;
    private final int minLevel;
    private final int maxLevel;
    private final String urlPrefix;
    private final String includeRegex;
    private final String excludeRegex;
    private final boolean outlineNumbering;

    public OutlineRenderer(String cssClass, int minLevel, int maxLevel, String urlPrefix, String includeRegex,
                           String excludeRegex, boolean outlineNumbering) {
        this.cssClass = cssClass;
        this.minLevel = minLevel;
        this.maxLevel = maxLevel;
        this.urlPrefix = urlPrefix;
        this.includeRegex = includeRegex;
        this.excludeRegex = excludeRegex;
        this.outlineNumbering = outlineNumbering;
    }

    public String render(DocumentOutline outline, OutputHandler handler) throws IOException {
        final StringBuilder buffer = new StringBuilder(1000); // wild guess

        final String cssClass = appendCssClass(handler, buffer);

        // TODO convert this to StAX, and the output handlers too
        buffer.append("<div class='toc-macro");
        if (isNotBlank(cssClass)) {
            buffer.append(" ").append(htmlEncode(cssClass));
        }
        buffer.append("'>");
        handler.appendPrefix(buffer);

        int previousLevel = -1;
        int startLevel = -1;
        final Deque<Integer> outlineStack = new LinkedList<Integer>();
        int outlineNumber = 0;
        final Iterator<Heading> headingIterator = outline.iterator(minLevel, maxLevel, includeRegex, excludeRegex);

        while (headingIterator.hasNext()) {
            final Heading heading = headingIterator.next();

            if (previousLevel == -1) {
                // initialise previousLevel based on the effective level of the first item found
                startLevel = previousLevel = heading.getEffectiveLevel();
            } else {
                // Increment the handler the correct number of times for the level of this item
                for (; previousLevel < heading.getEffectiveLevel(); previousLevel++) {
                    handler.appendIncLevel(buffer);

                    if (outlineNumbering) {
                        outlineNumber = pushOutline(outlineNumber, outlineStack);
                    }
                }

                // Decrement the handler the correct number of times for the level of this item
                for (; previousLevel > heading.getEffectiveLevel(); previousLevel--) {
                    handler.appendDecLevel(buffer);

                    if (outlineNumbering) {
                        outlineNumber = popOutline(outlineStack);
                    }
                }
            }

            outlineNumber++;

            handler.appendSeparator(buffer);

            handler.appendHeading(buffer, buildItem(outlineStack, outlineNumber, heading));
        }

        // take care of closing any <ul> that are currently opened
        while (previousLevel > startLevel) {
            previousLevel--;
            handler.appendDecLevel(buffer);

            if (outlineNumbering && previousLevel > startLevel) {
                popOutline(outlineStack);
            }
        }

        handler.appendPostfix(buffer);
        buffer.append("</div>");

        return buffer.toString();
    }

    private String buildItem(final Deque<Integer> outlineStack, final int outlineNumber, final Heading heading) {
        final StringBuilder item = new StringBuilder();
        if (outlineNumbering) {
            appendOutline(item, outlineStack, outlineNumber);
        }

        if (isNotBlank(heading.getAnchor())) {
            item.append("<a href='").append(urlPrefix).append("#").append(htmlEncode(heading.getAnchor())).append("'>");
        }

        item.append(htmlEncode(heading.getName()));
        if (isNotBlank(heading.getAnchor())) {
            item.append("</a>");
        }

        return item.toString();
    }

    private String appendCssClass(final OutputHandler handler, final StringBuilder buffer) throws IOException {
        if (isBlank(cssClass)) {
            return handler.appendStyle(buffer);
        } else {
            return cssClass;
        }
    }

    private static int popOutline(Deque<Integer> outlineStack) {
        return outlineStack.pop();
    }

    private static int pushOutline(int itemNum, Deque<Integer> outlineStack) {
        if (itemNum == 0) {
            itemNum = 1;
        }
        outlineStack.push(itemNum);
        return 0;
    }

    private static void appendOutline(StringBuilder out, Deque<Integer> outlineStack, int itemNum) {
        out.append("<span class='TOCOutline'>");
        final Iterator<Integer> iter = outlineStack.descendingIterator();
        while (iter.hasNext()) {
            out.append(iter.next()).append('.');
        }
        out.append(itemNum).append("</span> ");
    }

}
