/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.customware.confluence.plugin.toc;

import java.io.IOException;

/**
 * Provides an abstraction for different style of output.
 *
 * @author David Peterson
 */
public interface OutputHandler {

    /**
     * This is called at the beginning to allow output of any style-based code. If there is a class specified, this will
     * not be called at all.
     *
     * @param out The buffer to append to.
     * @return The base style classname.
     */
    String appendStyle(Appendable out) throws IOException;

    /**
     * This is called each time the levels should increase one level. If the level jumps from 1 to 3, this will be
     * called twice before any of the item methods are called.
     *
     * @param out The buffer to output to.
     */
    void appendIncLevel(Appendable out) throws IOException;

    /**
     * This is called each time the levels should decrease one level.
     */
    void appendDecLevel(Appendable out) throws IOException;

    /**
     * This is called at the beginning of the output.
     *
     * @param out The output buffer.
     */
    void appendPrefix(Appendable out) throws IOException;

    /**
     * This is called once at the end of the output.
     *
     * @param out The output buffer.
     */
    void appendPostfix(Appendable out) throws IOException;

    /**
     * This is called between each item.
     *
     * @param out The output buffer.
     */
    void appendSeparator(Appendable out) throws IOException;

    /**
     * This is called for each heading.
     *
     * @param out    The output buffer.
     * @param string The heading text.
     */
    void appendHeading(Appendable out, String string) throws IOException;

}
