/**
 *
 */
package net.customware.confluence.plugin.toc;

import java.util.Iterator;

/**
 * A representation of a document based on it's heading structure.
 */
public interface DocumentOutline {
    /**
     * Return an iterator over the entire outline. If you consider the outline a tree then this iterator will be
     * performing a depth first traversal. In simple terms, the iterator will return entries in the natural order you
     * would expect for a table of contents.
     *
     * @return a non thread safe Iterator over the entire outline, in order from highest level down to lowest child for each level.
     */
    Iterator<Heading> iterator();

    /**
     * Return an iterator with the same traversal order as described in {@link #iterator()}. The returned entries will be filtered based on the parameters supplied.
     * You should note that the includeRegex is applied before the exclude regex. So a heading that matches both include and exclude regexes will find itself
     * excluded.
     *
     * @param minLevel the minimum level to return headings for with 1 being the lowest (meaning all)
     * @param maxLevel the maximum level to return headings for, inclusive of the value set. If you want no maxLevel supply {@link Integer#MAX_VALUE}.
     * @param includeRegex a regex that must be matched for a Heading to be included in the iteration, or null if none
     * @param excludeRegex a regex that if matched means the heading will be excluded, or null if none

     * @return a non thread safe Iterator over the outline filtered as specified.
     */
    Iterator<Heading> iterator(int minLevel, int maxLevel, String includeRegex, String excludeRegex);

    public static interface Heading {
        /**
         * @return the name of this heading
         */
        public String getName();

        /**
         * @return the anchor for this heading. May be null.
         */
        public String getAnchor();

        /**
         * The effective level is the level of the outline a heading is at when missing levels have
         * been removed.
         *
         * @return the level this heading is at with 1 being the highest level
         */
        public int getEffectiveLevel();

        /**
         * @return the type of the heading, e.g. an h1 will be type 1, and h2 will be type 2, etc
         */
        public int getType();

        /**
         * @return the number of children this entry has.
         */
        public int getChildCount();
    }
}
