package net.customware.confluence.plugin.toc;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * In older versions of Confluence where wiki storage format was used, any regexes used in macro parameters had to avoid certain characters.
 * So a regex that needed to use a pipe character would instead use a comma. And if a comma was required then the escape code x2C would be
 * used instead. This is no longer necessary now that XHTML is used as the storage format and so the special usages are migrated back to
 * standard Java regular expressions.
 */
public class MigrateAwayWikiCompatibleRegexParameters implements MacroMigration {
    private static Pattern COMMA_TO_PIPE_REPLACE_PATTERN = Pattern.compile("//|,\\s*");
    private static Pattern ESCAPED_COMMA_TO_COMMA_REPLACE_PATTERN = Pattern.compile("\\\\x2[c|C]");

    private MacroMigration richTextMacroMigration;

    public MigrateAwayWikiCompatibleRegexParameters(@Qualifier("richTextMacroMigration") MacroMigration richTextMacroMigration) {
        this.richTextMacroMigration = richTextMacroMigration;
    }

    @Override
    public MacroDefinition migrate(MacroDefinition macroDefinition, ConversionContext context) {
        macroDefinition = richTextMacroMigration.migrate(macroDefinition, context);

        Map<String, String> params = macroDefinition.getParameters();

        String paramValue = params.get(AbstractTOCMacro.INCLUDE_PARAM);
        if (StringUtils.isNotBlank(paramValue))
            params.put(AbstractTOCMacro.INCLUDE_PARAM, convertRegex(paramValue));

        paramValue = params.get(AbstractTOCMacro.EXCLUDE_PARAM);
        if (StringUtils.isNotBlank(paramValue))
            params.put(AbstractTOCMacro.EXCLUDE_PARAM, convertRegex(paramValue));

        macroDefinition.setParameters(params);

        return macroDefinition;
    }

    /**
     * Replace the special characters in the supplied pattern and return the result.
     *
     * @param pattern the regex that may contain a TOC macro specific regex.
     * @return a standard Java regex.
     */
    private static String convertRegex(String pattern) {
        pattern = COMMA_TO_PIPE_REPLACE_PATTERN.matcher(pattern).replaceAll("|");
        pattern = ESCAPED_COMMA_TO_COMMA_REPLACE_PATTERN.matcher(pattern).replaceAll(",");
        return pattern;
    }

}
