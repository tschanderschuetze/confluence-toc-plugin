package net.customware.confluence.plugin.toc;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.ConversionContextOutputType;
import com.atlassian.confluence.renderer.PageContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

/**
 * Enumerations the two different TOCMacro implementations - client and server
 */
enum TocMacroImplementationType {
    CLIENT,
    SERVER;

    // Defined in MobileMacroManager in the confluence-mobile plugin
    static final String MOBILE_OUTPUT_DEVICE_TYPE = "mobile";

    // Defined in ConversionContextPropertyName in core
    static final String CONTAINED_RENDER = "containedRender";

    private static final Logger log = LoggerFactory.getLogger(TOCMacro.class);

    /**
     * Selects the appropriate TOCMacro implementation based on the execution context.
     */
    @Nonnull
    static TocMacroImplementationType selectImplementation(ConversionContext conversionContext) {
        if (!ConversionContextOutputType.DISPLAY.value().equals(conversionContext.getOutputType())) {
            // If the context is not for "DISPLAY" output, then immediately fall back to the server-side implementation
            return SERVER;
        } else if (MOBILE_OUTPUT_DEVICE_TYPE.equals(conversionContext.getOutputDeviceType())) {
            // The client-side implementation doesn't (yet) work on mobile browsers. It probably should, though.
            return SERVER;
        } else {
            // Otherwise we check if the client mode should work
            return useClientMode(conversionContext) ? CLIENT : SERVER;
        }
    }

    private static boolean useClientMode(ConversionContext conversionContext) {
        /*
            If we're inside something like an include macro or excerpt-include macro, then we don't want to use the
            client-side implementation since no headers or extra headers from the containing page will be there.

            The old way to detect that, which for now is maintained for backwards compatibility
            is by seeing if the "original" page context differs from the current page context.

            As those contexts are basically deprecated the new way should be to set the
            ConversionContextPropertyName.CONTAINED_RENDER
        */

        boolean isContainedRender = (boolean) conversionContext.getProperty(CONTAINED_RENDER, false);

        if (isContainedRender) {
            return false;
        }

        PageContext pageContext = conversionContext.getPageContext();
        boolean oldContextCheck = pageContext.getOriginalContext() == pageContext;

        if (!oldContextCheck) {
            log.debug("Using deprecated pageContext.getOriginalContext() to determine render mode for TOC macro, "
                    + "please use CONTAINED_RENDER property in ConversionContext");
        }

        return oldContextCheck;
    }

    ImplementationTypeSelectionEvent createEvent() {
        return new ImplementationTypeSelectionEvent(this);
    }


    @EventName("confluence.toc-macro.implementation")
    public static class ImplementationTypeSelectionEvent {
        private final TocMacroImplementationType implementationType;

        ImplementationTypeSelectionEvent(final TocMacroImplementationType implementationType) {
            this.implementationType = implementationType;
        }

        public String getImplementationType() {
            return implementationType.toString();
        }
    }

}
