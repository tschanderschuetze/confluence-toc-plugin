package net.customware.confluence.plugin.toc;

import net.customware.confluence.plugin.toc.DefaultDocumentOutlineBuilder.BuildableHeading;
import net.customware.confluence.plugin.toc.DocumentOutline.Heading;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class DocumentOutlineImplTest {
    @Test
    public void testIteratorWithEmptyOutline() {
        DocumentOutlineImpl outline = new DocumentOutlineImpl(Collections.<BuildableHeading>emptyList());
        Iterator<Heading> iterator = outline.iterator();

        assertIteratorEnded(iterator);

        // ensure hasNext is consistent
        assertFalse(iterator.hasNext());
    }

    @Test
    public void testIteratorOverRightSidedOutline() {
        Iterator<Heading> iterator = buildRightSidedOutline().iterator();

        assertFullRightSidedOutline(iterator);
    }

    @Test
    public void testIterateOverLeftSidedOutline() {
        Iterator<Heading> iterator = buildLeftSidedOutline().iterator();

        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-2,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-4,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3,heading-2", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-2,heading-2", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-2,heading-3", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-2", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-3", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-4", iterator.next().getName());

        assertIteratorEnded(iterator);

        assertFalse(iterator.hasNext());
    }

    @Test
    public void testIterateOverDistributedOutline() {
        Iterator<Heading> iterator = buildDistributedOutline().iterator();

        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-2", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-2a,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3a,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-2a,heading-2", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-2a,heading-3", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-3", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-4", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-2b,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3b,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3b,heading-2", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3b,heading-3", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-4,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-4,heading-2", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-5", iterator.next().getName());

        assertIteratorEnded(iterator);

        assertFalse(iterator.hasNext());
    }

    @Test
    public void testIterateOverSingleItemOutline() {
        Iterator<Heading> iterator = (new DocumentOutlineImpl(Collections.singletonList(new BuildableHeading(
                "monkey trousers", null, 1)))).iterator();

        assertTrue(iterator.hasNext());
        assertEquals("monkey trousers", iterator.next().getName());

        assertIteratorEnded(iterator);
    }

    /**
     * Only one item at each level in the tree
     */
    @Test
    public void testIterateOverSingleItemOutlineTree() {
        Iterator<Heading> iterator = buildSingleItemTree().iterator();

        assertTrue(iterator.hasNext());
        assertEquals("apple", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("banana", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("pear", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("sausage fruit", iterator.next().getName());

        assertIteratorEnded(iterator);
    }

    @Test
    public void testIterateOverPlaceholderOutline() {
        Iterator<Heading> iterator = buildPlaceHolderOutline().iterator();

        assertTrue(iterator.hasNext());
        assertEquals("level-2,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-2,heading-3", iterator.next().getName());

        assertIteratorEnded(iterator);
    }

    @Test
    public void testBadLevelConstraintArguments() {
        try {
            buildSingleItemTree().iterator(5, 2, null, null);
            fail("The minLevel should be enforced to be larged than the maxLevel parameter.");
        } catch (IllegalArgumentException ex) {
            // expected
        }
    }

    @Test
    public void testLevelConstraintIteratorWithEmptyOutline() {
        DocumentOutlineImpl outline = new DocumentOutlineImpl(Collections.<BuildableHeading>emptyList());
        Iterator<Heading> iterator = outline.iterator(2, 3, null, null);

        assertIteratorEnded(iterator);

        // ensure hasNext is consistent
        assertFalse(iterator.hasNext());
    }

    @Test
    public void testNoMatchesForLevelConstraintOverRightSidedOutline() {
        DocumentOutlineImpl outline = buildRightSidedOutline();
        Iterator<Heading> iterator = outline.iterator(6, 9, null, null);

        assertIteratorEnded(iterator);

        // ensure hasNext is consistent
        assertFalse(iterator.hasNext());
    }

    @Test
    public void testNoMatchesForLevelConstraintOverLeftSidedOutline() {
        DocumentOutlineImpl outline = buildLeftSidedOutline();
        Iterator<Heading> iterator = outline.iterator(12, 23, null, null);

        assertIteratorEnded(iterator);

        // ensure hasNext is consistent
        assertFalse(iterator.hasNext());
    }

    @Test
    public void testNoMatchesForLevelConstraintOverDistributedOutline() {
        DocumentOutlineImpl outline = buildDistributedOutline();
        Iterator<Heading> iterator = outline.iterator(8, 9, null, null);

        assertIteratorEnded(iterator);

        // ensure hasNext is consistent
        assertFalse(iterator.hasNext());
    }

    @Test
    public void testLevelConstraintOverRightSidedOutline() {
        DocumentOutlineImpl outline = buildRightSidedOutline();
        Iterator<Heading> iterator = outline.iterator(3, 10, null, null);

        assertTrue(iterator.hasNext());
        assertEquals("level-3,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3,heading-2", iterator.next().getName());

        assertIteratorEnded(iterator);
    }

    @Test
    public void testLevelConstraintOverLeftSidedOutline() {
        DocumentOutlineImpl outline = buildLeftSidedOutline();
        Iterator<Heading> iterator = outline.iterator(2, 3, null, null);

        assertTrue(iterator.hasNext());
        assertEquals("level-2,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3,heading-2", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-2,heading-2", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-2,heading-3", iterator.next().getName());

        assertIteratorEnded(iterator);
    }

    @Test
    public void testLevelConstraintOverDistributedOutline() {
        DocumentOutlineImpl outline = buildDistributedOutline();
        Iterator<Heading> iterator = outline.iterator(1, 1, null, null);

        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-2", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-3", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-4", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-5", iterator.next().getName());

        assertIteratorEnded(iterator);
    }

    @Test
    public void testLevelConstraintAppliesNoConstraint() {
        DocumentOutline outline = buildRightSidedOutline();
        Iterator<Heading> iterator = outline.iterator(1, Integer.MAX_VALUE, null, null);

        assertFullRightSidedOutline(iterator);
    }

    @Test
    public void testMatchingIncludeConstraintOnly() {
        DocumentOutline outline = buildDistributedOutline();
        Iterator<Heading> iterator = outline.iterator(1, Integer.MAX_VALUE, ".*1$", null);

        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-2a,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3a,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-2b,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3b,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-4,heading-1", iterator.next().getName());

        assertIteratorEnded(iterator);
    }

    @Test
    public void testMatchingIncludeConstraintAndLevelConstraint() {
        DocumentOutline outline = buildDistributedOutline();
        Iterator<Heading> iterator = outline.iterator(2, 3, ".*1$", null);

        assertTrue(iterator.hasNext());
        assertEquals("level-2a,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3a,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-2b,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3b,heading-1", iterator.next().getName());

        assertIteratorEnded(iterator);
    }

    @Test
    public void testNonMatchingIncludeConstraint() {
        DocumentOutline outline = buildRightSidedOutline();
        Iterator<Heading> iterator = outline.iterator(1, Integer.MAX_VALUE, "monkeytrousers", null);

        assertIteratorEnded(iterator);
    }

    @Test
    public void testMatchingExcludeConstraintOnly() {
        DocumentOutline outline = buildLeftSidedOutline();
        Iterator<Heading> iterator = outline.iterator(1, Integer.MAX_VALUE, null, "level-[1|2|3].*");

        assertTrue(iterator.hasNext());
        assertEquals("level-4,heading-1", iterator.next().getName());

        assertIteratorEnded(iterator);
    }

    @Test
    public void testMatchingExcludeConstraintAndLevelConstraint() {
        DocumentOutline outline = buildLeftSidedOutline();
        Iterator<Heading> iterator = outline.iterator(1, 3, null, "level-[2].*");

        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3,heading-2", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-2", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-3", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-4", iterator.next().getName());

        assertIteratorEnded(iterator);
    }

    private static void assertFullRightSidedOutline(Iterator<Heading> iterator) {
        assertTrue(iterator.hasNext());
        assertTrue(iterator.hasNext()); // ensure hasNext doesn't move us on or anything stupid

        assertEquals("level-1,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-2", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-1,heading-3", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-2,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-2,heading-2", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-2,heading-3", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-2,heading-4", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3,heading-1", iterator.next().getName());
        assertTrue(iterator.hasNext());
        assertEquals("level-3,heading-2", iterator.next().getName());

        assertIteratorEnded(iterator);

        assertFalse(iterator.hasNext());
    }

    /**
     * Build a test outline where only the last child at each level has further children (if any)
     *
     * @return
     */
    private static DocumentOutlineImpl buildRightSidedOutline() {
        List<BuildableHeading> headings = new ArrayList<BuildableHeading>(3);
        headings.add(new BuildableHeading("level-1,heading-1", null, 1));
        headings.add(new BuildableHeading("level-1,heading-2", null, 1));
        BuildableHeading headingWithChildren = new BuildableHeading("level-1,heading-3", null, 1);
        headings.add(headingWithChildren);
        headingWithChildren.addChild(new BuildableHeading("level-2,heading-1", null, 2));
        headingWithChildren.addChild(new BuildableHeading("level-2,heading-2", null, 2));
        headingWithChildren.addChild(new BuildableHeading("level-2,heading-3", null, 2));
        BuildableHeading headingWithChildren2 = new BuildableHeading("level-2,heading-4", null, 2);
        headingWithChildren.addChild(headingWithChildren2);
        headingWithChildren2.addChild(new BuildableHeading("level-3,heading-1", null, 3));
        headingWithChildren2.addChild(new BuildableHeading("level-3,heading-2", null, 3));

        return new DocumentOutlineImpl(headings);
    }

    /**
     * Build a test outline where only the first child at each level has further children (if any)
     *
     * @return
     */
    private static DocumentOutlineImpl buildLeftSidedOutline() {
        List<BuildableHeading> headings = new ArrayList<BuildableHeading>(4);
        BuildableHeading firstLevelParent = new BuildableHeading("level-1,heading-1", null, 1);
        headings.add(firstLevelParent);
        headings.add(new BuildableHeading("level-1,heading-2", null, 1));
        headings.add(new BuildableHeading("level-1,heading-3", null, 1));
        headings.add(new BuildableHeading("level-1,heading-4", null, 1));

        BuildableHeading secondLevelParent = new BuildableHeading("level-2,heading-1", null, 2);
        firstLevelParent.addChild(secondLevelParent);
        firstLevelParent.addChild(new BuildableHeading("level-2,heading-2", null, 2));
        firstLevelParent.addChild(new BuildableHeading("level-2,heading-3", null, 2));

        BuildableHeading thirdLevelParent = new BuildableHeading("level-3,heading-1", null, 3);
        secondLevelParent.addChild(thirdLevelParent);
        secondLevelParent.addChild(new BuildableHeading("level-3,heading-2", null, 3));

        thirdLevelParent.addChild(new BuildableHeading("level-4,heading-1", null, 4));

        return new DocumentOutlineImpl(headings);
    }

    /**
     * Build an outline with various branches in various places.
     *
     * @return
     */
    private static DocumentOutlineImpl buildDistributedOutline() {
        List<BuildableHeading> headings = new ArrayList<BuildableHeading>(5);
        headings.add(new BuildableHeading("level-1,heading-1", null, 1));
        headings.add(new BuildableHeading("level-1,heading-2", null, 1));
        headings.add(new BuildableHeading("level-1,heading-3", null, 1));
        headings.add(new BuildableHeading("level-1,heading-4", null, 1));
        headings.add(new BuildableHeading("level-1,heading-5", null, 1));

        BuildableHeading level2FirstParent = new BuildableHeading("level-2a,heading-1", null, 2);
        headings.get(1).addChild(level2FirstParent);
        headings.get(1).addChild(new BuildableHeading("level-2a,heading-2", null, 2));
        headings.get(1).addChild(new BuildableHeading("level-2a,heading-3", null, 2));

        level2FirstParent.addChild(new BuildableHeading("level-3a,heading-1", null, 3));

        BuildableHeading level2SecondParent = new BuildableHeading("level-2b,heading-1", null, 2);
        headings.get(3).addChild(level2SecondParent);
        level2SecondParent.addChild(new BuildableHeading("level-3b,heading-1", null, 3));
        level2SecondParent.addChild(new BuildableHeading("level-3b,heading-2", null, 3));
        BuildableHeading level3Parent = new BuildableHeading("level-3b,heading-3", null, 3);
        level2SecondParent.addChild(level3Parent);

        level3Parent.addChild(new BuildableHeading("level-4,heading-1", null, 4));
        level3Parent.addChild(new BuildableHeading("level-4,heading-2", null, 4));

        return new DocumentOutlineImpl(headings);
    }

    /**
     * Build an outline that has place holders (which are the equivalent of skipping a level in the outline i.e. going
     * from an h1 straight to an h4.
     *
     * @return
     */
    private static DocumentOutlineImpl buildPlaceHolderOutline() {
        List<BuildableHeading> headings = Collections.singletonList(new BuildableHeading(null, null, 0)); //top node is a placeholder
        headings.get(0).addChild(new BuildableHeading("level-2,heading-1", null, 2));
        headings.get(0).addChild(new BuildableHeading(null, null, 0));
        headings.get(0).addChild(new BuildableHeading("level-2,heading-3", null, 2));
        headings.get(0).getChild(1).addChild(new BuildableHeading("level-3,heading-1", null, 3));

        return new DocumentOutlineImpl(headings);
    }

    private static DocumentOutlineImpl buildSingleItemTree() {
        List<BuildableHeading> headings = new ArrayList<BuildableHeading>(1);

        headings.add(new BuildableHeading("apple", null, 1));
        headings.get(0).addChild(new BuildableHeading("banana", null, 2));
        headings.get(0).getChild(0).addChild(new BuildableHeading("pear", null, 3));
        headings.get(0).getChild(0).getChild(0).addChild(new BuildableHeading("sausage fruit", null, 4));

        return new DocumentOutlineImpl(headings);
    }

    private static void assertIteratorEnded(Iterator<Heading> iterator) {
        assertFalse(iterator.hasNext());
        try {
            iterator.next();
            fail("An exception should be thrown since there is no next element.");
        } catch (NoSuchElementException ex) {
            // expected
        }
    }
}
