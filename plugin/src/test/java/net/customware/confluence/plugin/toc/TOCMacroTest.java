package net.customware.confluence.plugin.toc;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultHtmlToXmlConverter;
import com.atlassian.confluence.content.render.xhtml.DefaultXmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.DelegateXmlOutputFactory;
import com.atlassian.confluence.content.render.xhtml.HtmlToXmlConverter;
import com.atlassian.confluence.core.BodyContent;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.template.TemplateRenderer;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.renderer.RenderContextOutputType;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.io.CharStreams;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.xml.stream.XMLOutputFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import static com.ctc.wstx.api.WstxOutputProperties.P_AUTOMATIC_END_ELEMENTS;
import static com.ctc.wstx.api.WstxOutputProperties.P_OUTPUT_VALIDATE_STRUCTURE;
import static java.lang.Boolean.FALSE;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TOCMacroTest {

    private static final String PAGE_TITLE = "Page Title";

    protected static final String RENDERED_CONTENT = "<h2><a name='RenderedContent'></a>Rendered Content</h2>";

    private TOCMacro tocMacro;

    @Mock
    private XhtmlContent xhtmlContent;

    @Mock
    private HtmlToXmlConverter htmlToXmlConverter;

    @Mock
    private SettingsManager settingsManager;

    @Mock
    private LocaleManager localeManager;

    @Mock
    private I18NBeanFactory i18nBeanFactory;

    @Mock
    private TemplateRenderer templateRenderer;

    @Mock
    private EventPublisher eventPublisher;

    @Mock(answer = RETURNS_DEEP_STUBS)
    private PageBuilderService pageBuilderService;

    private StaxDocumentOutlineCreator staxDocumentOutlineCreator;

    @Before
    public void setUp() throws Exception {
        staxDocumentOutlineCreator = new StaxDocumentOutlineCreator(new DefaultXmlEventReaderFactory(), new DelegateXmlOutputFactory(xmlOutputFactory()));

        tocMacro = new TOCMacro(staxDocumentOutlineCreator, xhtmlContent, htmlToXmlConverter, settingsManager, localeManager, i18nBeanFactory, pageBuilderService, templateRenderer, eventPublisher) {
            @Override
            protected OutlineRenderer getOutlineRenderer(String className, int minLevel, int maxLevel, String prefix, String include,
                                                         String exclude, boolean outlineNumbering) {
                return new OutlineRenderer(EMPTY, 1, 7, EMPTY, EMPTY, EMPTY, true);
            }

            @Override
            protected ListHandler createListHandler(String indent, String style) {
                return super.createListHandler(indent, null);
            }
        };

        tocMacro.setBeingRendered(false);
    }

    private static XMLOutputFactory xmlOutputFactory() {
        XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
        xmlOutputFactory.setProperty(P_OUTPUT_VALIDATE_STRUCTURE, FALSE);
        xmlOutputFactory.setProperty(P_AUTOMATIC_END_ELEMENTS, FALSE);
        return xmlOutputFactory;
    }

    private static String getTestHtml(final String resourcePath) throws IOException {

        try (Reader in = new BufferedReader(new InputStreamReader(TOCMacroTest.class.getClassLoader().getResourceAsStream(resourcePath)));
             Writer out = new StringWriter()) {
            CharStreams.copy(in, out);

            return normaliseWhiteSpace(out.toString());
        }
    }

    private static String normaliseWhiteSpace(String str) {
        return str.replaceAll("\\s+", " ").trim();
    }

    @Test
    public void testCreateOutput() {
        assertEquals("TOC", tocMacro.createOutput(null, null, "TOC"));
    }

    @Test
    public void testTocDoesNotRenderItselfRecursively() throws Exception {
        final Map<String, String> macroParams = new HashMap<String, String>();
        final Page page = new Page();

        String testContent = "<h1><a name=\"TestToC2-Level1\" ></a>Foo</h1>";
        when(xhtmlContent.convertStorageToView(EMPTY, new DefaultConversionContext(page.toPageContext()))).thenReturn(testContent);
        when(htmlToXmlConverter.convert(testContent)).thenReturn(testContent);

        /* Execute the macro using a new RenderContext in each time */
        assertTrue(isNotBlank(tocMacro.execute(macroParams, EMPTY, new PageContext(page))));
        /* Should return blank on subsequent rendering, even if a new RenderContext is passed to the macro */
        tocMacro.setBeingRendered(true);
        assertTrue(isBlank(tocMacro.execute(macroParams, EMPTY, new PageContext(page))));
    }

    /**
     * <a href="http://jira.developer.atlassian.com/browse/TOC-47">TOC-47</a>
     *
     * @throws IOException    Thrown if there is a problem reading test data.
     * @throws MacroException Thrown if the macro being tested throws it.
     */
    @Test
    public void testTocIsNotUnneccessarilyIndentedWhenPageHeadingsStartFromSmallerHeadings() throws Exception {
        final String html = getTestHtml("net/customware/confluence/plugin/toc/headings-starting-from-h3.html");
        final Map<String, String> macroParams = new HashMap<String, String>();
        final Page page = new Page();
        final PageContext pageContext = new PageContext(page);
        final ConversionContext conversionContext = new DefaultConversionContext(pageContext);
        final String actualMacroOutput;
        final String expectedMacroOutput;

        when(xhtmlContent.convertStorageToView(EMPTY, conversionContext)).thenReturn(html);
        when(htmlToXmlConverter.convert(html)).thenReturn(html);

        macroParams.put("outline", Boolean.TRUE.toString()); /* We want to make sure the outline has the correct numbering too */

        actualMacroOutput = normaliseWhiteSpace(tocMacro.execute(macroParams, EMPTY, conversionContext));
        expectedMacroOutput = getTestHtml("net/customware/confluence/plugin/toc/headings-starting-from-h3-result.html");

        assertEquals(expectedMacroOutput, actualMacroOutput);
    }


    /**
     * <a href="http://jira.developer.atlassian.com/browse/TOC-47">TOC-47</a>
     *
     * @throws IOException    Thrown if there is a problem reading test data.
     * @throws MacroException Thrown if the macro being tested throws it.
     */
    @Test
    public void testTocOutlineProperlyCalculatedForFlatTocWhenPageHeadingsStartFromSmallerHeadings() throws Exception {
        final String html = getTestHtml("net/customware/confluence/plugin/toc/headings-starting-from-h3.html");
        final Map<String, String> macroParams = new HashMap<String, String>();
        final Page page = new Page();
        final PageContext pageContext = new PageContext(page);
        final ConversionContext conversionContext = new DefaultConversionContext(pageContext);
        final String actualMacroOutput;
        final String expectedMacroOutput;

        when(xhtmlContent.convertStorageToView(EMPTY, conversionContext)).thenReturn(html);
        when(htmlToXmlConverter.convert(html)).thenReturn(html);

        macroParams.put("type", "flat"); /* Flat toc type */
        macroParams.put("outline", Boolean.TRUE.toString()); /* We wanna make sure the outline has the correct numbering too */

        actualMacroOutput = tocMacro.execute(macroParams, EMPTY, pageContext);
        expectedMacroOutput = getTestHtml("net/customware/confluence/plugin/toc/headings-starting-from-h3-flat-result.html");

        assertEquals(expectedMacroOutput, actualMacroOutput);
    }


    /**
     * <a href="http://jira.developer.atlassian.com/browse/TOC-47">TOC-47</a>
     *
     * @throws IOException    Thrown if there is a problem reading test data.
     * @throws MacroException Thrown if the macro being tested throws it.
     */
    @Test
    public void testTocIsNotUnneccessarilyIndentedWhenPageHeadingsAreNotSequential() throws Exception {
        final String html = getTestHtml("net/customware/confluence/plugin/toc/headings-not-sequential.html");
        final Map<String, String> macroParams = new HashMap<String, String>();
        final Page page = new Page();
        final PageContext pageContext = new PageContext(page);
        final ConversionContext conversionContext = new DefaultConversionContext(pageContext);
        final String actualMacroOutput;
        final String expectedMacroOutput;

        when(xhtmlContent.convertStorageToView(EMPTY, conversionContext)).thenReturn(html);
        when(htmlToXmlConverter.convert(html)).thenReturn(html);

        macroParams.put("outline", Boolean.TRUE.toString()); /* We wanna make sure the outline has the correct numbering too */

        actualMacroOutput = normaliseWhiteSpace(tocMacro.execute(macroParams, EMPTY, pageContext));
        expectedMacroOutput = getTestHtml("net/customware/confluence/plugin/toc/headings-not-sequential-result.html");

        assertEquals(expectedMacroOutput, actualMacroOutput);
    }

    /**
     * <a href="http://jira.developer.atlassian.com/browse/TOC-47">TOC-47</a>
     *
     * @throws IOException    Thrown if there is a problem reading test data.
     * @throws MacroException Thrown if the macro being tested throws it.
     */
    @Test
    public void testTocOutlineProperlyCalculatedForFlatTocWhenHeadingsAreNotSequential() throws Exception {
        final String html = getTestHtml("net/customware/confluence/plugin/toc/headings-not-sequential.html");
        final Map<String, String> macroParams = new HashMap<String, String>();
        final Page page = new Page();
        final PageContext pageContext = new PageContext(page);
        final ConversionContext conversionContext = new DefaultConversionContext(pageContext);
        final String actualMacroOutput;
        final String expectedMacroOutput;

        when(xhtmlContent.convertStorageToView(EMPTY, conversionContext)).thenReturn(html);
        when(htmlToXmlConverter.convert(html)).thenReturn(html);

        macroParams.put("type", "flat"); /* Flat toc type */
        macroParams.put("outline", Boolean.TRUE.toString()); /* We wanna make sure the outline has the correct numbering too */

        actualMacroOutput = tocMacro.execute(macroParams, EMPTY, pageContext);
        expectedMacroOutput = getTestHtml("net/customware/confluence/plugin/toc/headings-not-sequential-flat-result.html");

        assertEquals(expectedMacroOutput, actualMacroOutput);
    }

    @Test
    public void testGetContentFromPage() throws Exception {
        Map<String, String> params = new HashMap<String, String>();
        String body = null;

        Page page = new Page();
        page.setTitle(PAGE_TITLE);
        BodyContent bodyContent = new BodyContent();
        bodyContent.setBody(RENDERED_CONTENT);
        page.setBodyContent(bodyContent);

        PageContext ctx = new PageContext(page);

        when(xhtmlContent.convertStorageToView(RENDERED_CONTENT, new DefaultConversionContext(ctx))).thenReturn(RENDERED_CONTENT);

        assertEquals(RENDERED_CONTENT, tocMacro.getContent(params, body, new DefaultConversionContext(ctx)));
    }

    @Test
    public void testGetDefaultType() {
        assertEquals(TOCMacro.LIST_TYPE, tocMacro.getDefaultType());
    }

    @Test
    public void testExecuteUnprintable() {
        try {
            final Map<String, String> macroParams = new HashMap<String, String>();
            macroParams.put("printable", FALSE.toString());
            Page page = new Page();
            PageContext ctx = new PageContext(page);
            when(ctx.getOutputType()).thenReturn(RenderContextOutputType.DISPLAY);

            assertEquals("", tocMacro.execute(macroParams, null, new DefaultConversionContext(ctx)));

        } catch (final MacroExecutionException me) {
            fail("Unexpected MacroException: " + me.getMessage());
        }
    }

    @Test
    public void testWithUnprintableParam() throws Exception {
        final Map<String, String> macroParams = new HashMap<String, String>();
        macroParams.put("printable", FALSE.toString());
        PageContext ctx = new PageContext(new Page());
        ctx.setOutputType("display");
        String output = tocMacro.execute(macroParams, null, new DefaultConversionContext(ctx));
        assertTrue(isBlank(output));
    }

    @Test
    public void testGetName() {
        assertEquals("toc", tocMacro.getName());
    }

    /* TOC-72 */
    @Test
    public void testParseErroneousHtml() throws Exception {
        final String unparsableHtml = getErroneousHtml();
        final Map<String, String> macroParameters = new HashMap<String, String>();
        final Page pageToBeRendered = new Page();
        final String expectedToc = "<div class='toc-macro'>\n" +
                "<ul class='toc-indentation'>\n" +
                "<li><a href='#TOC-72-Important'>Important</a></li>\n" +
                "<li><a href='#TOC-72-AlsoImportant'>Also Important</a>\n" +
                "<ul class='toc-indentation'>\n" +
                "<li><a href='#TOC-72-FairlyImportant'>Fairly Important</a></li>\n" +
                "<li><a href='#TOC-72-SomewhatImportant'>Somewhat Important</a></li>\n" +
                "</ul>\n" +
                "</li>\n" +
                "<li><a href='#TOC-72-QuiteImportant'>Quite Important</a></li>\n" +
                "<li><a href='#TOC-72-Urgent'>Urgent</a></li>\n" +
                "</ul>\n" +
                "</div>";

        macroParameters.put("maxLevel", "3");

        pageToBeRendered.setSpace(new Space("TST"));
        pageToBeRendered.setTitle("testParseErroneousHtml");
        pageToBeRendered.setBodyAsString(EMPTY);

        HtmlToXmlConverter htmlToXmlConverter = new DefaultHtmlToXmlConverter();
        tocMacro = new TOCMacro(staxDocumentOutlineCreator, xhtmlContent, htmlToXmlConverter, settingsManager, localeManager, i18nBeanFactory, pageBuilderService, templateRenderer, eventPublisher) {
            @Override
            protected OutlineRenderer getOutlineRenderer(String className, int minLevel, int maxLevel, String prefix, String include,
                                                         String exclude, boolean outlineNumbering) {
                return new OutlineRenderer(EMPTY, 1, 3, EMPTY, EMPTY, EMPTY, false);
            }

            @Override
            protected ListHandler createListHandler(String indent, String style) {
                return super.createListHandler(indent, null);
            }
        };

        when(xhtmlContent.convertStorageToView(EMPTY, new DefaultConversionContext(pageToBeRendered.toPageContext()))).thenReturn(unparsableHtml);

        assertEquals(expectedToc, tocMacro.execute(macroParameters, EMPTY, new DefaultConversionContext(pageToBeRendered.toPageContext())));
    }

    private static String getErroneousHtml() throws IOException {
        Writer writer = null;
        Reader reader = null;

        try {
            writer = new StringWriter();
            reader = new BufferedReader(new InputStreamReader(TOCMacroTest.class.getClassLoader().getResourceAsStream("net/customware/confluence/plugin/toc/erroneous-html.html")));

            CharStreams.copy(reader, writer);

            return writer.toString();

        } finally {
            reader.close();
            writer.close();
        }
    }

    /*
     * TOC-70
     */
    @Test
    public void testDetectionOfInfiniteLoopInRenderingNotDoneWithHttpRequestAttributes() throws Exception {
        final Map<String, String> macroParameters = new HashMap<String, String>();
        final Page pageToBeRendered = new Page();

        macroParameters.put("maxLevel", String.valueOf(3));

        pageToBeRendered.setSpace(new Space("TST"));
        pageToBeRendered.setTitle("TOC-70");
        pageToBeRendered.setBodyAsString(EMPTY);

        when(xhtmlContent.convertStorageToView(EMPTY, new DefaultConversionContext(pageToBeRendered.toPageContext()))).thenReturn(EMPTY);

        assertEquals(EMPTY, tocMacro.execute(macroParameters, null, new DefaultConversionContext(pageToBeRendered.toPageContext())));

    }
}
