package net.customware.confluence.plugin.toc;

import net.customware.confluence.plugin.toc.DocumentOutline.Heading;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;

public class EmptyLevelTrimmingDocumentOutlineBuilderTest {
    private EmptyLevelTrimmingDocumentOutlineBuilder builder;

    @Before
    public void setUp() throws Exception {
        builder = new EmptyLevelTrimmingDocumentOutlineBuilder();
    }

    @Test
    public void testNoMissingLevels() {
        Iterator<Heading> iterator = builder.add("l1-h1", null, 1).add("l1-h2", null, 1).nextLevel().add("l2-h1",
                null, 2).getDocumentOutline().iterator();

        Heading heading = iterator.next();
        assertEquals("l1-h1", heading.getName());
        assertEquals(1, heading.getEffectiveLevel());
        assertEquals(0, heading.getChildCount());

        heading = iterator.next();
        assertEquals("l1-h2", heading.getName());
        assertEquals(1, heading.getEffectiveLevel());
        assertEquals(1, heading.getChildCount());

        heading = iterator.next();
        assertEquals("l2-h1", heading.getName());
        assertEquals(2, heading.getEffectiveLevel());
        assertEquals(0, heading.getChildCount());
    }

    /**
     * Test the builder when the top level of the outline is unused.
     */
    @Test
    public void testMissingTopLevel() {
        builder.nextLevel();
        Iterator<Heading> iterator = builder.add("l2-h1", "a-l2-h1", 2).add("l2-h2", "a-l2-h2", 2).nextLevel().add("l3-h1",
                "a-l3-h1", 3).getDocumentOutline().iterator();

        Heading heading = iterator.next();
        assertEquals("l2-h1", heading.getName());
        assertEquals(1, heading.getEffectiveLevel());
        assertEquals(0, heading.getChildCount());

        heading = iterator.next();
        assertEquals("l2-h2", heading.getName());
        assertEquals(1, heading.getEffectiveLevel());
        assertEquals(1, heading.getChildCount());

        heading = iterator.next();
        assertEquals("l3-h1", heading.getName());
        assertEquals(2, heading.getEffectiveLevel());
        assertEquals(0, heading.getChildCount());
    }

    @Test
    public void testMissingTopCoupleOfLevels() {
        builder.nextLevel();
        builder.nextLevel();
        Iterator<Heading> iterator = builder.add("1", null, 1).nextLevel().add("1-1", null, 2).nextLevel().add("1-1-1", null, 3)
                .previousLevel().add("1-2", null, 2).nextLevel().add("1-2-1", null, 3).previousLevel().previousLevel()
                .add("2", null, 1).getDocumentOutline().iterator();

        Heading heading = iterator.next();
        assertEquals("1", heading.getName());
        assertEquals(1, heading.getEffectiveLevel());
        assertEquals(2, heading.getChildCount());

        heading = iterator.next();
        assertEquals("1-1", heading.getName());
        assertEquals(2, heading.getEffectiveLevel());
        assertEquals(1, heading.getChildCount());

        heading = iterator.next();
        assertEquals("1-1-1", heading.getName());
        assertEquals(3, heading.getEffectiveLevel());
        assertEquals(0, heading.getChildCount());

        heading = iterator.next();
        assertEquals("1-2", heading.getName());
        assertEquals(2, heading.getEffectiveLevel());
        assertEquals(1, heading.getChildCount());

        heading = iterator.next();
        assertEquals("1-2-1", heading.getName());
        assertEquals(3, heading.getEffectiveLevel());
        assertEquals(0, heading.getChildCount());

        heading = iterator.next();
        assertEquals("2", heading.getName());
        assertEquals(1, heading.getEffectiveLevel());
        assertEquals(0, heading.getChildCount());
    }

    @Test
    public void testMissingIntermediateLevels() {
        Iterator<Heading> iterator = builder.add("1", null, 1).nextLevel().nextLevel().nextLevel().add("1-1", null, 4)
                .nextLevel().add("1-1-1", null, 5).previousLevel().add("1-2", null, 4).nextLevel().add("1-2-1", null, 5)
                .previousLevel().previousLevel().previousLevel().previousLevel().add("2", null, 1).getDocumentOutline()
                .iterator();

        Heading heading = iterator.next();
        assertEquals("1", heading.getName());
        assertEquals(1, heading.getEffectiveLevel());
        assertEquals(1, heading.getType());
        assertEquals(2, heading.getChildCount());

        heading = iterator.next();
        assertEquals("1-1", heading.getName());
        assertEquals(2, heading.getEffectiveLevel());
        assertEquals(4, heading.getType());
        assertEquals(1, heading.getChildCount());

        heading = iterator.next();
        assertEquals("1-1-1", heading.getName());
        assertEquals(3, heading.getEffectiveLevel());
        assertEquals(5, heading.getType());
        assertEquals(0, heading.getChildCount());

        heading = iterator.next();
        assertEquals("1-2", heading.getName());
        assertEquals(2, heading.getEffectiveLevel());
        assertEquals(4, heading.getType());
        assertEquals(1, heading.getChildCount());

        heading = iterator.next();
        assertEquals("1-2-1", heading.getName());
        assertEquals(3, heading.getEffectiveLevel());
        assertEquals(5, heading.getType());
        assertEquals(0, heading.getChildCount());

        heading = iterator.next();
        assertEquals("2", heading.getName());
        assertEquals(1, heading.getEffectiveLevel());
        assertEquals(1, heading.getType());
        assertEquals(0, heading.getChildCount());
    }
}
