package net.customware.confluence.plugin.toc;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.renderer.PageContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.Writer;

import static com.atlassian.renderer.RenderContextOutputType.DISPLAY;
import static com.atlassian.renderer.RenderContextOutputType.PDF;
import static com.atlassian.renderer.RenderContextOutputType.WORD;
import static net.customware.confluence.plugin.toc.TocMacroImplementationType.CONTAINED_RENDER;
import static net.customware.confluence.plugin.toc.TocMacroImplementationType.MOBILE_OUTPUT_DEVICE_TYPE;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TocMacroImplementationTypeTest {
    @Mock
    ConversionContext conversionContext;

    @Mock
    PageContext pageContext1, pageContext2;

    @Mock
    Writer writer;

    @Test
    public void clientSideImplementationIsSelectedWhenPageContextsAreNotNested() throws Exception {
        when(conversionContext.getOutputType()).thenReturn(DISPLAY);
        when(conversionContext.getPageContext()).thenReturn(pageContext1);
        when(conversionContext.getProperty(CONTAINED_RENDER, false)).thenReturn(false);
        when(pageContext1.getOriginalContext()).thenReturn(pageContext1);

        assertThat(TocMacroImplementationType.selectImplementation(conversionContext), is(TocMacroImplementationType.CLIENT));
    }

    @Test
    public void serverSideImplementationIsSelectedWhenPageContextsAreNested() throws Exception {
        when(conversionContext.getOutputType()).thenReturn(DISPLAY);
        when(conversionContext.getPageContext()).thenReturn(pageContext1);
        when(conversionContext.getProperty(CONTAINED_RENDER, false)).thenReturn(false);
        when(pageContext1.getOriginalContext()).thenReturn(pageContext2);

        assertThat(TocMacroImplementationType.selectImplementation(conversionContext), is(TocMacroImplementationType.SERVER));
    }

    @Test
    public void serverSideImplementationIsSelectedWhenContainedRenderPropertySet() throws Exception {
        when(conversionContext.getOutputType()).thenReturn(DISPLAY);
        when(conversionContext.getProperty(CONTAINED_RENDER, false)).thenReturn(true);

        assertThat(TocMacroImplementationType.selectImplementation(conversionContext), is(TocMacroImplementationType.SERVER));
    }

    @Test
    public void serverSideImplementationIsSelectedWhenContextIsPdf() throws Exception {
        when(conversionContext.getOutputType()).thenReturn(PDF);
        assertThat(TocMacroImplementationType.selectImplementation(conversionContext), is(TocMacroImplementationType.SERVER));
    }

    @Test
    public void serverSideImplementationIsSelectedWhenContextIsWord() throws Exception {
        when(conversionContext.getOutputType()).thenReturn(WORD);
        assertThat(TocMacroImplementationType.selectImplementation(conversionContext), is(TocMacroImplementationType.SERVER));
    }

    @Test
    public void serverSideImplementationIsSelectedWhenTargetDeviceIsMobile() throws Exception {
        when(conversionContext.getOutputType()).thenReturn(DISPLAY);
        when(conversionContext.getOutputDeviceType()).thenReturn(MOBILE_OUTPUT_DEVICE_TYPE);
        assertThat(TocMacroImplementationType.selectImplementation(conversionContext), is(TocMacroImplementationType.SERVER));
    }
}