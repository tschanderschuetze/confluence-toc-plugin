package it.webdriver;

import com.atlassian.confluence.webdriver.pageobjects.component.ConfluenceAbstractPageComponent;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import it.net.customware.confluence.plugin.toc.Heading;

import java.net.URI;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import static org.openqa.selenium.By.className;
import static org.openqa.selenium.By.xpath;

public class TableOfContents extends ConfluenceAbstractPageComponent {
    private final Style style;

    enum Style {
        LIST {
            @Override
            List<Heading> getHeadings(final TableOfContents toc) {
                return getListTocHeadings(toc.tocContainer, toc.pageBinder);
            }
        },
        FLAT {
            @Override
            List<Heading> getHeadings(final TableOfContents toc) {
                return getFlatTocHeadings(toc.tocContainer, toc.pageBinder);
            }
        };

        abstract List<Heading> getHeadings(TableOfContents toc);
    }

    @ElementBy(className = "toc-macro")
    private PageElement tocContainer;

    public TableOfContents(final Style style) {
        this.style = style;
    }

    public List<Heading> getHeadings() {
        return style.getHeadings(this);
    }

    public PageElement getTocContainer() {
        return tocContainer;
    }

    /**
     * The TOC is assembled in its entirety before being added to the browser DOM, so once it's visible we can assume
     * it's fully-formed, so we don't have to use wait conditions from here on.
     */
    @WaitUntil
    public void waitUntilTocRenders() {
        waitUntilTrue(tocContainer.timed().isVisible());
    }


    public static class TocItem extends ConfluenceAbstractPageComponent {
        private final PageElement itemContainer;
        private final PageElement itemBody;

        public TocItem(final PageElement itemContainer, final PageElement itemBody) {
            this.itemContainer = itemContainer;
            this.itemBody = itemBody;
        }

        Heading toHeading() {
            return new Heading(trimToNull(getOutline().getText()), getLinkText(), getLinkTarget(), getChildItems());
        }

        private PageElement getLink() {
            return itemBody.find(className("toc-link"));
        }

        private String getLinkText() {
            return getLink().getText();
        }

        private String getLinkTarget() {
            final String href = getLink().getAttribute("href");
            return URI.create(requireNonNull(href)).getFragment();
        }

        private PageElement getOutline() {
            return itemBody.find(className("toc-outline"));
        }

        private List<Heading> getChildItems() {
            return getListTocHeadings(itemContainer, pageBinder);
        }
    }

    private static List<Heading> getListTocHeadings(PageElement container, final PageBinder binder) {
        final List<PageElement> listItems = container.findAll(xpath("ul/li"));
        return listItems.stream()
                .map(listItem -> {
                    final PageElement itemBody = listItem.find(className("toc-item-body"));
                    return binder.bind(TocItem.class, listItem, itemBody).toHeading();
                }).collect(toList());
    }

    private static List<Heading> getFlatTocHeadings(PageElement container, final PageBinder binder) {
        final List<PageElement> flatItems = container.findAll(className("toc-item-body"));
        return flatItems.stream()
                .map(flatItem -> binder.bind(TocItem.class, flatItem, flatItem).toHeading())
                .collect(toList());
    }
}
