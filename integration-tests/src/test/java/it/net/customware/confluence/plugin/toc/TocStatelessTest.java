package it.net.customware.confluence.plugin.toc;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.api.model.content.Space;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.DashboardPage;
import com.atlassian.confluence.webdriver.pageobjects.page.content.EditContentPage;
import com.atlassian.confluence.webdriver.pageobjects.page.content.ViewPage;
import com.atlassian.pageobjects.elements.GlobalElementFinder;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;
import static org.openqa.selenium.By.xpath;

@RunWith(ConfluenceStatelessTestRunner.class)
public class TocStatelessTest {

    @Inject
    private static ConfluenceTestedProduct product;
    @Inject
    private static ConfluenceRestClient restClient;
    @Inject
    private static GlobalElementFinder finder;

    @Fixture
    private static UserFixture user = userFixture().build();
    @Fixture
    private static SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .build();
    @Fixture
    private static PageFixture targetPage = pageFixture()
            .space(space)
            .author(user)
            .title("Target page")
            .content("Target Page Content")
            .build();

    @Rule
    public TestName testName = new TestName();

    @BeforeClass
    public static void initialise() {
        product.login(user.get(), DashboardPage.class);
    }

    @Test
    public void testCreateTocWithNonSequentialHeadersStartingFromH2() {
        final Content page = createContent(
                space.get(),
                "h2. Heading A\n" +
                        "{toc-zone:class=toc-zone}\n" +
                        "h4. Heading B\n" +
                        "h4. Heading C\n" +
                        "h6. Heading D\n" +
                        "{toc-zone}\n" +
                        "h2. Heading E\n" +
                        "h4. Heading F\n"
        );

        product.viewPage(page);

        // At the top
        verifyFlatHeadings(
                "//div[@class='wiki-content']//div[contains(@class,'toc-zone')][1]",
                asList(
                        new Heading("Heading B", format("#%s-HeadingB", page.getTitle())),
                        new Heading("Heading C", format("#%s-HeadingC", page.getTitle())),
                        new Heading("Heading D", format("#%s-HeadingD", page.getTitle()))
                )
        );

        waitUntilTrue(finder.find(xpath("//div[contains(@class,'toc-zone')]/following-sibling::h4")).timed().isPresent());

        // At the bottom
        verifyFlatHeadings(
                "//div[@class='wiki-content']//div[contains(@class,'toc-zone')][2]",
                asList(
                        new Heading("Heading B", format("#%s-HeadingB", page.getTitle())),
                        new Heading("Heading C", format("#%s-HeadingC", page.getTitle())),
                        new Heading("Heading D", format("#%s-HeadingD", page.getTitle()))
                )
        );
    }

    @Test
    public void testCreateTocAtTopLocation() {
        final Content page = createContent(
                space.get(),
                "h2. Heading A\n" +
                        "{toc-zone:class=toc-zone|location=top}\n" +
                        "h4. Heading B\n" +
                        "h4. Heading C\n" +
                        "h6. Heading D\n" +
                        "{toc-zone}\n" +
                        "h2. Heading E\n" +
                        "h4. Heading F\n"
        );

        product.viewPage(page);

        // At the top
        verifyFlatHeadings(
                "//div[contains(@class,'toc-zone')]",
                asList(
                        new Heading("Heading B", format("#%s-HeadingB", page.getTitle())),
                        new Heading("Heading C", format("#%s-HeadingC", page.getTitle())),
                        new Heading("Heading D", format("#%s-HeadingD", page.getTitle()))
                )
        );
        // Verify that a toc is not generated at the bottom
        waitUntilFalse(
                finder.find(xpath("//div[@class='wiki-content']/h4/following-sibling::div[@class='toc-zone']"))
                        .timed().isPresent()
        );
    }

    @Test
    public void testCreateTocAtBottomLocation() {
        final Content page = createContent(
                space.get(),
                "{toc-zone:class=toc-zone|location=bottom}\n" +
                        "h4. Heading B\n" +
                        "h4. Heading C\n" +
                        "h6. Heading D\n" +
                        "{toc-zone}\n" +
                        "h2. Heading E\n" +
                        "h4. Heading F\n"
        );

        product.viewPage(page);

        waitUntilFalse(
                finder.find(xpath("//div[contains(@class,'toc-zone')]/following-sibling::h6"))
                        .timed()
                        .isPresent()
        );

        // At the bottom
        verifyFlatHeadings(
                "//div[@class='wiki-content']//h4/following-sibling::div[contains(@class,'toc-zone')]",
                asList(
                        new Heading("Heading B", format("#%s-HeadingB", page.getTitle())),
                        new Heading("Heading C", format("#%s-HeadingC", page.getTitle())),
                        new Heading("Heading D", format("#%s-HeadingD", page.getTitle()))
                )
        );
    }

    @Test
    public void testIfWikiLinksInTocZoneBodyIsRefactoredIfTargetPageIsRenamed() {
        final Content page = createContent(
                space.get(),
                "{toc-zone:class=toc-zone}\n" +
                        "\n" +
                        "h1.Heading 1\n" +
                        "h2.Heading 2\n" +
                        "h3.Heading 3\n" +
                        "h4.[Target page|Target page]\n" +
                        "\n" +
                        "{toc-zone}"
        );

        ViewPage viewPage = product.viewPage(page);

        verifyFlatHeadings(
                "//div[@class='toc-macro toc-zone']",
                asList(
                        new Heading("Heading 1", format("#%s-Heading1", page.getTitle())),
                        new Heading("Heading 2", format("#%s-Heading2", page.getTitle())),
                        new Heading("Heading 3", format("#%s-Heading3", page.getTitle())),
                        new Heading(targetPage.get().getTitle(), format("#%s-Targetpage", page.getTitle()))
                )
        );

        // Rename page
        EditContentPage editContentPage = viewPage.editShortcut();
        editContentPage.setTitle("Test page renamed");
        editContentPage.save();

        verifyFlatHeadings(
                "//div[@class='toc-macro toc-zone']",
                asList(
                        new Heading("Heading 1", "#Testpagerenamed-Heading1"),
                        new Heading("Heading 2", "#Testpagerenamed-Heading2"),
                        new Heading("Heading 3", "#Testpagerenamed-Heading3"),
                        new Heading("Target page", "#Testpagerenamed-Targetpage")
                )
        );
    }

    @Test
    public void testThatTocMacroInIncludedPageOnlyRendersHeadersFromTheIncludedPage() {
        final Content page = createContent(
                space.get(),
                "{toc}\n" +
                        "h1. Heading 1\n" +
                        "h2. Heading 1.1\n" +
                        "h3. Heading 1.1.1"
        );

        final Content includePage = createContent(
                space.get(),
                format("Include Page for %s", page.getTitle()),
                "{include:" + page.getTitle() + "}\n" +
                        "h1. Heading 2"
        );

        product.viewPage(includePage);

        // Links should have containing page's anchor
        final String tagPrefix = format("#IncludePagefor%s", page.getTitle());
        verifyHeadings(
                "//div[contains(@class,'toc-macro')]/ul",
                singletonList(
                        new Heading("Heading 1", format("%s-Heading1", tagPrefix),
                                singletonList(
                                        new Heading("Heading 1.1", format("%s-Heading1.1", tagPrefix),
                                                singletonList(
                                                        new Heading("Heading 1.1.1", format("%s-Heading1.1.1", tagPrefix))
                                                )
                                        )
                                )
                        )
                )
        );
    }

    private void verifyFlatHeadings(String tocListXPath, List<Heading> headings) {
        int i = 0;
        waitUntilFalse(
                finder.find(xpath(tocListXPath + "/a[" + (headings.size() + 1) + "]")).timed().isPresent()
        );

        for (Heading heading : headings) {
            final String headingXPath = "(" + tocListXPath + "//a)[" + ++i + "]";

            waitUntil(
                    finder.find(xpath(headingXPath)).timed().getText(),
                    is(heading.text)
            );
            waitUntil(
                    finder.find(xpath(headingXPath)).timed().getAttribute("href"),
                    endsWith(heading.href)
            );

            if (isNotBlank(heading.outline)) {
                waitUntil(
                        finder.find(xpath(tocListXPath + "/span[@class='TOCOutline'][" + i + "]")).timed().getText(),
                        is(heading.outline)
                );
            }
        }
    }

    private void verifyHeadings(String tocListXPath, List<Heading> headings) {
        waitUntilFalse(
                finder.find(xpath(tocListXPath + "/li[" + (headings.size() + 1) + "]"))
                        .timed()
                        .isPresent()
        );

        for (int i = 0; i < headings.size(); ++i) {
            final String headingXPath = tocListXPath + "/li[" + (i + 1) + "]";
            final Heading heading = headings.get(i);

            if (null == heading.href) {
                final String expectedText = isBlank(heading.outline) ? heading.text : heading.outline + ' ' + heading.text;
                waitUntil(
                        finder.find(xpath(headingXPath)).timed().getText(),
                        is(expectedText)
                );
            } else {
                final String headingLinkXpath = headingXPath + "/a";
                waitUntil(
                        finder.find(xpath(headingLinkXpath)).timed().getText(),
                        is(heading.text)
                );
                waitUntil(
                        finder.find(xpath(headingLinkXpath)).timed().getAttribute("href"),
                        endsWith(heading.href)
                );
            }

            if (null != heading.outline) {
                waitUntil(
                        finder.find(xpath(headingXPath + "/span[@class='TOCOutline']")).timed().getText(),
                        is(heading.outline)
                );
            }

            if (heading.hasChildren()) {
                verifyHeadings(
                        headingXPath + "/a/following-sibling::ul[li]",
                        heading.children
                );
            }
        }
    }

    private Content createContent(final Space space, final String wikiBody) {
        return createContent(space, testName.getMethodName(), wikiBody);
    }

    private Content createContent(final Space space, final String title, final String wikiBody) {
        return restClient.createSession(user.get()).contentService().create(
                Content.builder()
                        .space(space)
                        .type(ContentType.PAGE)
                        .title(title)
                        .body(wikiBody, ContentRepresentation.WIKI)
                        .build()
        );
    }
}