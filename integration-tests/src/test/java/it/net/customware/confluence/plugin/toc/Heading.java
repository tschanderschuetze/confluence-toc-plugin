package it.net.customware.confluence.plugin.toc;

import com.google.common.base.Objects;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

public class Heading {
    final String outline;
    final String text;
    final String href;
    final List<Heading> children;

    public Heading(String text, String href) {
        this(null, text, href, emptyList());
    }

    public Heading(String text, String href, List<Heading> children) {
        this(null, text, href, children);
    }

    public Heading(String text, String href, Heading... children) {
        this(null, text, href, asList(children));
    }

    public Heading(String outline, String text, String href, Heading... children) {
        this(outline, text, href, asList(children));
    }

    public Heading(String outline, String text, String href, List<Heading> children) {
        this.outline = outline;
        this.href = href;
        this.text = text;
        this.children = children;
    }

    boolean hasChildren() {
        return null != children && !children.isEmpty();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Heading heading = (Heading) o;
        return Objects.equal(children, heading.children) &&
                Objects.equal(outline, heading.outline) &&
                Objects.equal(href, heading.href) &&
                Objects.equal(text, heading.text);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(outline);
    }

    @Override
    public String toString() {
        return "Heading{" +
                "outline='" + outline + '\'' +
                ", text='" + text + '\'' +
                ", href='" + href + '\'' +
                ", children=" + children +
                '}';
    }
}
